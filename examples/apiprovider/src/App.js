import React, { createContext, useCallback, useState } from 'react'
import logo from './logo.svg'
import './App.css'

import { AuthProvider, ApiProvider, initialState, apiGet, useCustomApi, jsonHeaders } from './hooks/radbse-auth'

const RepositoryContext = createContext([])

const REPOSITORY__FETCH_REQUEST = 'REPOSITORY__FETCH_REQUEST'
const REPOSITORY__FETCH_SUCCESS = 'REPOSITORY__FETCH_SUCCESS'
const REPOSITORY__FETCH_FAILURE = 'REPOSITORY__FETCH_FAILURE'

const corsJsonHeaders = {
    ...jsonHeaders,
    headers: {
        ...jsonHeaders.headers,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers':
            'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-GitHub-OTP, X-Requested-With',
    },
}
const repositoryActionCreators = {
    getRepositories: apiGet(
        username => `https://api.github.com/users/${username}/repos`,
        [REPOSITORY__FETCH_REQUEST, REPOSITORY__FETCH_SUCCESS, REPOSITORY__FETCH_FAILURE],
        false,
        corsJsonHeaders
    ),
}

const repositoryReducer = (state, action) => {
    switch (action.type) {
        case REPOSITORY__FETCH_SUCCESS:
            return { ...state, ...action.response }

        default:
            return { ...state }
    }
}

const useRepoApi = () => useCustomApi(RepositoryContext)

const RepositoryList = () => {
    const [actions, useApi] = useRepoApi()
    const [repos, setRepos] = useState([])
    const [reposLoading, getRepos] = useApi(actions.getRepositories)

    const handleClick = useCallback(async () => {
        const result = await getRepos('codelinq')
        console.log(result)
        setRepos(result.response)
    }, [getRepos])

    return (
        <div>
            <div>
                <button onClick={handleClick}>Refresh</button>
            </div>
            <div>
                {repos.map(repo => (
                    <p>{repo.name}</p>
                ))}
            </div>
        </div>
    )
}

function App() {
    return (
        <AuthProvider initialState={initialState}>
            <ApiProvider
                initialState={[]}
                context={RepositoryContext}
                reducer={repositoryReducer}
                actionCreators={repositoryActionCreators}
            >
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                        <p>
                            Edit <code>src/App.js</code> and save to reload.
                        </p>
                        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                            Learn React
                        </a>
                        <RepositoryList />
                    </header>
                </div>
            </ApiProvider>
        </AuthProvider>
    )
}

export default App
