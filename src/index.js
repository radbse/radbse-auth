import React, { useContext, useState, useMemo, useReducer } from 'react'
import { bindActionCreators } from '@radbse/hooks'

export class ApiError extends Error {
    constructor(message, data) {
        super(message)
        this.error = data

        if (Error.captureStackTrace) Error.captureStackTrace(this, ApiError)
    }
}

export const initialState = { token: null }

export const AuthContext = React.createContext()

const AUTH__SET_TOKEN = 'AUTH__SET_TOKEN'

export const authActionCreators = {
    setToken: (token) => ({ type: AUTH__SET_TOKEN, token }),
}

export const authReducer = (state, action) => {
    switch (action.type) {
        case AUTH__SET_TOKEN:
            return { ...state, ...action.token }
        default:
            return { ...state }
    }
}

export const AuthProvider = ({
    initialState,
    children,
    reducer = authReducer,
    actionCreators = authActionCreators,
}) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    const providerValue = {
        state,
        actions: bindActionCreators(actionCreators, dispatch),
        dispatch,
    }
    return <AuthContext.Provider value={providerValue}>{children}</AuthContext.Provider>
}

export const jsonHeaders = {
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
}

export const apiCookiePost = (
    url,
    types,
    sendCookies = true,
    headers = jsonHeaders,
    noCache = true,
    sendAuthBearer = false
) => {
    return apiPost(url, types, sendAuthBearer, headers, noCache, sendCookies)
}

export const apiPost = (
    url,
    types,
    sendAuthBearer = true,
    headers = jsonHeaders,
    noCache = true,
    sendCookies = false
) => {
    return {
        url,
        types,
        method: 'post',
        headers,
        sendAuthBearer,
        sendCookies,
        noCache,
    }
}

export const apiCookieGet = (
    url,
    types,
    sendCookies = true,
    headers = jsonHeaders,
    noCache = true,
    sendAuthBearer = false
) => {
    return apiGet(url, types, sendAuthBearer, headers, noCache, sendCookies)
}

export const apiGet = (
    url,
    types,
    sendAuthBearer = true,
    headers = jsonHeaders,
    noCache = true,
    sendCookies = false
) => {
    return {
        url,
        types,
        method: 'get',
        headers,
        sendAuthBearer,
        sendCookies,
        noCache,
    }
}

export const apiCookiePut = (
    url,
    types,
    sendCookies = true,
    headers = jsonHeaders,
    noCache = true,
    sendAuthBearer = false
) => {
    return apiPut(url, types, sendAuthBearer, headers, noCache, sendCookies)
}

export const apiPut = (
    url,
    types,
    sendAuthBearer = true,
    headers = jsonHeaders,
    noCache = true,
    sendCookies = false
) => {
    return {
        url,
        types,
        method: 'put',
        headers,
        sendAuthBearer,
        sendCookies,
        noCache,
    }
}

export const apiCookieDelete = (
    url,
    types,
    sendCookies = true,
    headers = jsonHeaders,
    noCache = true,
    sendAuthBearer = false
) => {
    return apiDelete(url, types, sendAuthBearer, headers, noCache, sendCookies)
}

export const apiDelete = (
    url,
    types,
    sendAuthBearer = true,
    headers = jsonHeaders,
    noCache = true,
    sendCookies = false
) => {
    return {
        url,
        types,
        method: 'delete',
        headers,
        sendAuthBearer,
        sendCookies,
        noCache,
    }
}

export const useApi = (action, dispatch, authContext = AuthContext) => {
    const { state } = useContext(authContext)
    let token
    if (state) {
        token = state.token
    }
    const [loading, setLoading] = useState(false)

    const apiFn = useMemo(() => {
        const { url, types, method, headers, sendAuthBearer, noCache, sendCookies } = action

        let requestType = ''
        let successType = ''
        let failureType = ''

        if (dispatch && types) {
            if (!Array.isArray(types) || types.length !== 3 || !types.every((type) => typeof type === 'string')) {
                throw new Error('Expected an array of three string types.')
            }
            ;[requestType, successType, failureType] = types
        }

        let config = {
            method,
            ...headers,
        }

        if (sendAuthBearer && token) {
            const authHeader = { AUTHORIZATION: `Bearer ${token}` }
            config = { ...config, headers: { ...config.headers, ...authHeader } }
        }

        if (noCache) {
            config = {
                ...config,
                cache: 'no-cache',
                headers: {
                    ...config.headers,
                    ...{ pragma: 'no-cache', 'cache-control': 'no-cache' },
                },
            }
        }

        if (sendCookies) {
            config = {
                ...config,
                credentials: 'same-origin',
            }
        }

        return async (data = {}, freshToken) => {
            let requestUrl = url

            if (data) {
                if (typeof url === 'function') {
                    requestUrl = url(data)
                }

                if (method !== 'get') config = { ...config, body: JSON.stringify(data) }
            }

            if (typeof requestUrl !== 'string') throw new Error('Expected uri to be a string.')

            if (freshToken) {
                config.headers['AUTHORIZATION'] = `Bearer ${freshToken}`
            }

            setLoading(true)
            if (dispatch) dispatch({ ...data, type: requestType })

            try {
                const fetchResponse = await fetch(requestUrl, config)
                setLoading(false)
                const response = await fetchResponse.json()
                if (response.success === undefined) {
                    const action = { ...data, response, type: successType }
                    if (dispatch) dispatch(action)
                    return action
                }
                if (response.success) {
                    const action = {
                        payload: data,
                        response: response.result,
                        type: successType,
                    }
                    if (dispatch) dispatch(action)
                    return action
                }

                if (dispatch)
                    dispatch({
                        payload: data,
                        error: response.errors,
                        type: failureType,
                    })
                throw new ApiError('Server returned an error response', response.errors)
            } catch (error) {
                if (dispatch) dispatch({ payload: data, error, type: failureType })
                throw error
            }
        }
    }, [action, dispatch, token])

    return [loading, apiFn]
}

export const useAuth = () => useContext(AuthContext)

export const useAuthApi = () => {
    const { actions, dispatch } = useAuth()
    return [actions, (action) => useApi(action, dispatch, AuthContext)]
}

export const ApiProvider = ({ initialState, children, reducer, actionCreators, context: Context }) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    const providerValue = {
        state,
        actions: bindActionCreators(actionCreators, dispatch),
        dispatch,
    }
    return <Context.Provider value={providerValue}>{children}</Context.Provider>
}

export const useCustomApi = (context) => {
    const { actions, dispatch, state } = useContext(context)
    return [actions, (action) => useApi(action, dispatch, AuthContext), state]
}
